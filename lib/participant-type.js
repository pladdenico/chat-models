"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParticipantType;
(function (ParticipantType) {
    ParticipantType[ParticipantType["User"] = 0] = "User";
    ParticipantType[ParticipantType["Group"] = 1] = "Group";
})(ParticipantType = exports.ParticipantType || (exports.ParticipantType = {}));
//# sourceMappingURL=participant-type.js.map
export interface IMessage {
    id: number;
    conversationId: number;
    value: string;
    senderId: number;
}

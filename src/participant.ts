export interface IParticipant {
  id: number;
  name: string;
  type: string;
}

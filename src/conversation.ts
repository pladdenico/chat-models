export interface IConversation {
  id: number;
  name: string;
  memberIds: number[];
  messageIds: number[];
}
